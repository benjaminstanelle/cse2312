.global main
.func main
   
main:
   
		            @ branch to prompt procedure with return
    BL  _scanf              @ branch to scanf procedure with return
    MOV R5, R0	
                      
         
    BL _scanf
    MOV R6, R0
    BL _printf
    
    BL _divide

    BL _printfd
    B main              @ branch to exit procedure with no return
   

_printf:
    MOV R4, LR
    LDR R0, =printf_str
    MOV R1, R5
    MOV R2, R6
    BL printf
    MOV PC, R4
    
      
_printfd:
    PUSH {LR}               @ push LR to stack
    LDR R0, =printf_result     @ R0 contains formatted string address
    BL printf               @ call printf
    POP {PC}                @ pop LR from stack and return
    
_scanf:
    PUSH {LR}               @ store LR since scanf call overwrites
    SUB SP, SP, #4          @ make room on stack
    LDR R0, =format_str     @ R0 contains address of format string
    MOV R1, SP              @ move SP to R1 to store entry on stack
    BL scanf                @ call scanf
    LDR R0, [SP]            @ load value at SP into R0
    ADD SP, SP, #4          @ restore the stack pointer
    POP {PC}                @ return
_divide:
	PUSH {LR}
	VMOV S0, R5
        VCVT.F32.S32 S0, S0
	VMOV S1, R6 @moving ints into single precision float values
	VCVT.F32.S32 S1, S1
	
	VDIV.F32 S0, S0, S1 @Divide
	VCVT.F64.F32 D4, S0
	VMOV R1, R2, D4
        POP {PC}
.data
format_str:     .asciz      "%d"
printf_result:     .asciz      "%f\n"
printf_str:	.asciz      "%d / %d = "

