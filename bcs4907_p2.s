/******************************************************************************
* @NetID:bcs4907  		ID:1001534907
* @author Benjamin Stanelle
******************************************************************************/
 
.global main
.func main
   
main:
    BL _seedrand            @ seed random number generator with current time
    MOV R0, #0              @ initialze index variable
writeloop:
    CMP R0, #10             @ check to see if we are done iterating
    BEQ writedone           @ exit loop if done
    LDR R1, =a              @ get address of a
    LSL R2, R0, #2          @ multiply index*4 to get array offset
    ADD R2, R1, R2          @ R2 now has the element address
    
    PUSH {R0}               @ backup iterator before procedure call
    PUSH {R2}
    
               @ backup element address before procedure call
    BL _getrand             @ get a random number
    POP {R2}
    BL _mod		    @ call to mod s

    STR R0, [R2]            @ Store R0 to the address of to a[i]
    POP {R0} 
               @ restore iterator
    ADD R0, R0, #1          @ increment index
    B   writeloop           @ branch to next loop iteration
writedone:
    MOV R0, #0              @ initialze index variable
readloop:
    CMP R0, #10          @ check to see if we are done iterating
    BEQ readdone            @ exit loop if done
    LDR R1, =a              @ get address of a
    @LDR R9, =a
    LSL R2, R0, #2          @ multiply index*4 to get array offset
    ADD R2, R1, R2          @ R2 now has the element address
    MOV R8, R2
    LDR R1, [R2] 
 
    CMP R0, #1
    SUBGE R8, R8, #4
    LDRGE R9, [R8]
    	           
    PUSH {R0}               @ backup register before printf
    PUSH {R1}               @ backup register before printf
    PUSH {R2}               @ backup register before printf
    MOV R2, R1              @ move array value to R2 for printf
    MOV R1, R0   		 @ move array index to R1 for printf
    CMP R1, #1
    BLHS _MIN
@random:
    BLHS _MAX 
          
    BL  _printf             @ branch to print procedure with return
    

   @ BLLS _MAX

    POP {R2}                @ restore register
    POP {R1}                @ restore register
    POP {R0}                @ restore register
    ADD R0, R0, #1          @ increment index
    B   readloop            @ branch to next loop iteration
readdone:
    B _exit                 @ exit if done
    
_exit:  
    MOV R7, #4              @ write syscall, 4
    MOV R0, #1              @ output stream to monitor, 1
    MOV R2, #21             @ print string length
    LDR R1, =exit_str       @ string at label exit_str:
    SWI 0                   @ execute syscall
    MOV R7, #1              @ terminate syscall, 1
    SWI 0                   @ execute syscall
       
_printf:
    PUSH {LR}               @ store the return address
    LDR R0, =printf_str     @ R0 contains formatted string address
    BL printf               @ call printf
    POP {PC}                @ restore the stack pointer and return
    
_seedrand:
    PUSH {LR}               @ backup return address
    MOV R0, #0              @ pass 0 as argument to time call
    BL time                 @ get system time
    MOV R1, R0              @ pass sytem time as argument to srand
    BL srand                @ seed the random number generator
    POP {PC}                @ return 
    
_getrand:
    PUSH {LR}               @ backup return address
    BL rand                 @ get a random number
    POP {PC}                @ return 

_mod:
	MOV R1, R0
	LDR R4,=0x3E7
	
	CMP R4, R1
	MOVHS R0, R1
	MOVHS R1, R4
	MOVHS R4, R0

	MOV R0, #0
	B _modloopcheck
_modloop:
	ADD R0, R0, #1
	SUB R1, R1, R4
_modloopcheck:
	CMP R1, R4
	BHS _modloop
  	MOV R0, R1
	MOV PC, LR

_max_print:
    PUSH {LR}               @ store the return address
    LDR R0, =max_str 
    MOV R1, R6		    @ R0 contains formatted string address
    BL printf               @ call printf
    POP {PC} 

_min_print:
    PUSH {LR}               @ store the return address
    LDR R0, =min_str     @ R0 contains formatted string address
    MOV R1, R6
    
    BL printf               @ call printf
    B _MAX
    POP {PC} 

_MIN:
	CMP R1, #1		
	MOVEQ R6, R9
	
	CMP R9, R2 		
	MOVHS R5, R2
	MOVLO R5, R9

	CMP R5, R6     		
        MOVLO R6, R5
		
	PUSH {R1}
	PUSH {R2}
	PUSH {R6}
	CMP R1, #9
	BLEQ _min_print
	POP {R6}
	POP {R2}
	POP {R1}
	
	MOV PC, LR

_MAX:
	CMP R1, #1		
	MOVEQ R6, R9
	
	CMP R9, R2 		
	MOVLO R5, R2
	MOVHS R5, R9

	CMP R5, R6     		
        MOVHS R6, R5

	PUSH {R1}
	PUSH {R2}
	PUSH {R6}

	CMP R1, #9		
	BLEQ _max_print

	POP {R6}
	POP {R2}
	POP {R1}
	
	B _exit

.data

.balign 4
a:              .skip       40
printf_str:     .asciz      "a[%d] = %d\n"
debug_str:
.asciz "R%-2d   0x%08X  %011d \n"
exit_str:       .ascii      "Terminating program.\n"
max_str:	.asciz "MAXIMUM VALUE = %d\n"
min_str:	.asciz "MINIMUM VALUE = %d\n"
