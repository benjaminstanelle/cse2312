#include <stdio.h>

extern unsigned int endian_func(int a);

int main ()
{
    unsigned int a;
    int b,d;
    int array_a[3];
    scanf("%X",&array_a[0]);
    scanf("%X",&array_a[1]);
    scanf("%X",&array_a[2]);

  for(int i=0;i<sizeof(array_a)/sizeof(array_a[0]);i++)
  {
      a=array_a[i];
      printf("array_a[%d] =  ",i);
	/* loop through 32 bits, shift that binary value of the hex number
	 inputed right by b check to see if the bits of the shift value equal 1*/
      for (b = 31; b >= 0; b--) 
    {
        d = a >> b; 
        if (d & 1){ 	/*bitwise and of LSB and 1*/
        printf("1");
	}
        else {
        printf("0");
	}
    }
     printf("   %d    %X\n",array_a[i],array_a[i]);
  }
	printf("After Endian Conversion: \n");
	
for(int i=0;i<sizeof(array_a)/sizeof(array_a[0]);i++)
  {
      a=endian_func(array_a[i]);
      printf("array_a[%d] =  ",i);
      for (b = 31; b >= 0; b--)
    {
        d = a >> b;
        if (d & 1){ /*bitwise and*/
        printf("1");
	}
        else {
        printf("0");
	}
    }


     printf("   %u    %X\n",a,a);
  }

	
    return 0;
}