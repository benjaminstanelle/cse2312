/******************************************************************************
* @file scanf.s
* @brief simple scanf example
*
* Simple example of invoking scanf to retrieve a number from keyboard input
*
* @Benjamin stanelle
******************************************************************************/
 
.global main
.func main
   
main:
    BL  _scanf              @ branch to scanf procedure with return
    MOV R5, R0              @ move return value R0 to argument register R1
    BL  _scanf  
    MOV R6, R0

    MOV R0, #0
    MOV R1, R5  @n
    MOV R2, R6  @m

    BL _count_partitions
    MOV R1, R0    @number of partitions
    
    PUSH {R1}
    PUSH {R2} 
    PUSH {R3}
    MOV R2, R5
    MOV R3, R6
    BL  _printf 

    POP {R3}   
    POP {R2}
    POP {R1}
            
    B   main               @ branch to exit procedure with no return
   

       
_printf:
    PUSH {LR}              @ store LR since printf call overwrites
    LDR R0, =printf_str     @ R0 contains formatted string address
    MOV R1, R1              @ R1 contains printf argument (redundant line)
    BL printf               @ call printf
    POP {PC}             @ return
    
_scanf:
    PUSH {LR}                @ store LR since scanf call overwrites
    SUB SP, SP, #4          @ make room on stack
    LDR R0, =format_str     @ R0 contains address of format string
    MOV R1, SP              @ move SP to R1 to store entry on stack
    BL scanf                @ call scanf
    LDR R0, [SP]            @ load value at SP into R0
    ADD SP, SP, #4          @ restore the stack pointer
    POP {PC}                 @ return

_count_partitions:
	PUSH {LR}    @if (n==0) return 1
	CMP R1, #0
	MOVEQ R0, #1
	POPEQ {PC}

	CMP R1, #0    @else if (n<0) return 0;
	MOVLT R0, #0
	POPLT {PC}

	CMP R2, #0   @ else if (m==0) return 0
	MOVEQ R0, #0
	POPEQ {PC}

	PUSH {R1}
	PUSH {R2}
	SUB R2, R2, #1  @m-1 n is the same
	
	BL _count_partitions

	POP {R2}
	POP {R1}
	SUB R1, R1, R2

	PUSH {R0}
	BL _count_partitions
	POP {R4}  @
	ADD R0, R0, R4 @Add all the partitions which are in R4

        @STR LR, [SP, #-4]!
	@STR R0, [SP, #-4]!
	
	POP {PC}

 
	@ADD SP, SP, #-8 @allocating two spots on stack
	@STR LR, [SP, #4] @push/save the current link register into 1st word
	@STR R2, [SP] @push/ save current value of R2 into second word
	@ADD SP, SP, #8 @deallocating
	
	
.data
format_str:     .asciz      "%d"
printf_str:     .asciz      "There are %d, partitions using %d integers up to %d\n"

