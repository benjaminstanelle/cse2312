/* Benjamin Stanelle	bcs4907		1001534907*/

.data
result_str:	.asciz "Result = %d\n"
number_str:	.asciz      "%d"
read_char:	.ascii      " "




.global main
.func main

main: 
	loop:

	BL _scanf
	MOV R1, R0

	BL _getchar
	MOV R3, R0

	BL _scanf
	MOV R2, R0

	CMP R3, #'m'
	BLEQ _MIN			@Branch Link if char R3 == 'm'
	MOV R1, R0

	CMP R3, #'+'
	BLEQ _SUM
	MOV R1, R0
	
	CMP R3, #'-'
	BLEQ _DIFFERENCE
	MOV R1, R0

	CMP R3, #'<'
	BLEQ _SHIFT_LEFT
	MOV R1, R0
	
	BL _print_val
	
	B loop

_SUM: 
	MOV R0, R1
	ADD R0, R2
	MOV PC, LR
	

_DIFFERENCE:
	MOV R0, R1
	SUB R0, R2
	MOV PC, LR
_MIN:
	MOV R0, R1
	CMP R0, R2
	MOVGE R0, R2

	MOV PC, LR
	

_SHIFT_LEFT:
	MOV R0, R1, LSL R2
	MOV PC, LR

_print_val:
	MOV R4, LR
	LDR R0, =result_str
	BL printf  			@ call printf, where R1 is the print argument
	MOV LR, R4          
	MOV PC, LR 

_scanf:
    PUSH {LR}               
    SUB SP, SP, #4         
    LDR R0, =number_str     
    MOV R1, SP        			@Does this interfere with R1 Value taken later?     
    BL scanf               
    LDR R0, [SP]           
    ADD SP, SP, #4        
    POP {PC} 

_getchar:
    MOV R7, #3              
    MOV R0, #0              
    MOV R2, #1              
    LDR R1, =read_char     
    SWI 0                   
    LDR R0, [R1]            
    AND R0, #0xFF           
    MOV PC, LR              
  
